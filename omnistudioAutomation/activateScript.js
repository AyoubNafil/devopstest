const puppeteer = require('puppeteer');
const { execSync } = require('child_process');
const fs = require('fs');
const xml2js = require('xml2js');

let pathToDeltaFolder = process.argv.slice(2);
//let orgName = process.argv.slice(3)[0];


const packageNamespace = 'vlocity_cmt__';
const managedPackageRuntimeDisabled = process.env.managedPackageRuntimeEnabled !== undefined ? process.env.managedPackageRuntimeEnabled : true;

async function getComponentMapFromXML() {

    //console.log(pathToDeltaFolder)
    //console.log(orgName)
    try {
        // Read the XML file content
        const xmlContent = fs.readFileSync(`../${pathToDeltaFolder}/package/package.xml`, 'utf8');

        const parser = new xml2js.Parser();

        // Parse XML to JSON
        const result = await parser.parseStringPromise(xmlContent);

        const typesArray = result.Package.types;

        let componentMap = {};

        typesArray.forEach(type => {
            if (type.name && type.name[0] && type.members) {
                componentMap[type.name[0]] = type.members;
            }
        });

        return componentMap;
    } catch (err) {
        //console.log("Error parsing XML:", err);
        return {};
    }
}

async function compileOSAndFlexCards() {

    const componentMap = await getComponentMapFromXML();

    // Specify the path to your JSON file
    //const jsonFilePath = 'MyOrgDesc.json';
    
    // Read the content of the JSON file
    //const rawOutput = fs.readFileSync(jsonFilePath, 'utf8');
    
    // Run the sfdx command
    
    
    const rawOutput = execSync(`sf org display --verbose --json`).toString('utf8');
    let rawOutputParsed = JSON.parse(rawOutput);
    let instanceUrl = rawOutputParsed.result.instanceUrl;
    let accessToken = rawOutputParsed.result.accessToken;

    let omniScriptsQueryCondition = "";
    let omniUiCardsQueryCondition = "";
    let omniScripts;
    let flexCards;

    // If delta has OmniScript reference then fetch the id from the name of the omniscript component
    if (componentMap.hasOwnProperty("OmniScript") && componentMap["OmniScript"].length > 0) {
        const omniScriptNames = componentMap["OmniScript"].map(name => `'${name}'`).join(',');
        omniScriptsQueryCondition = `AND UniqueName IN (${omniScriptNames})`;

        // Using sf query on OmniProcess for OmniScripts
        const queryOmniscript = `SELECT Id, UniqueName FROM OmniProcess WHERE IsActive = true AND IsIntegrationProcedure = false ${omniScriptsQueryCondition}`;
        const rawOutputFromOSQuery =  execSync(`sf data query --query "${queryOmniscript}" --json`)
        omniScripts = JSON.parse(rawOutputFromOSQuery).result;
    }

    // If delta has FlexCard reference then fetch the id from the name of the FlexCard component
    if (componentMap.hasOwnProperty("OmniUiCard") && componentMap["OmniUiCard"].length > 0) {
        const omniUiCardNames = componentMap["OmniUiCard"].map(name => `'${name}'`).join(',');
        omniUiCardsQueryCondition = `AND UniqueName IN (${omniUiCardNames})`;

        // Using sf query on OmniUiCard for FlexCards
        const queryFlexCard = `SELECT Id, UniqueName FROM OmniUiCard WHERE IsActive = true ${omniUiCardsQueryCondition}`;
        const rawOutputFromFlexCardQuery =  execSync(`sf data query --query "${queryFlexCard}" --json`)
        flexCards = JSON.parse(rawOutputFromFlexCardQuery).result;
    }


    //Initialize puppeteer options
    const puppeteerOptions = {
        headless: process.env.puppeteerHeadless !== undefined ? process.env.puppeteerHeadless : true,
        args: ['--no-sandbox', '--disable-setuid-sandbox']
    };

    let browser;
    try {
        let siteUrl = instanceUrl;
        let loginURl = `${siteUrl}/secur/frontdoor.jsp?sid=${accessToken}`;
        try {
            browser = await puppeteer.launch(puppeteerOptions);
        } catch (error) {
            console.log('Puppeteer initialization failed:', error);
            process.exit(1); // Exit with a non-zero exit code to signal failure
        }

        const page = await browser.newPage();
        const loginTimeout = 60000; // You may adjust this value as needed.

        await Promise.all([
            page.waitForNavigation({ timeout: loginTimeout, waitUntil: 'load' }),
            page.waitForNavigation({ timeout: loginTimeout, waitUntil: 'networkidle2' }),
            page.goto(loginURl, { timeout: loginTimeout })
        ]);

        const omniScriptIds = omniScripts !== undefined ? omniScripts.records.map(record => record.Id) : [];
        const flexCardsIds = flexCards !== undefined ? flexCards.records.map(record => record.Id) : [];

        //Loop in Omniscripts and active
        for(const eachOmniscriptId of omniScriptIds){
            let omniScriptDisignerpageLink = siteUrl + '/apex/' + packageNamespace + 'OmniLwcCompile?id=' + eachOmniscriptId + '&activate=true';
            await page.goto(omniScriptDisignerpageLink);
            await page.waitForTimeout(5000);

            let tries = 0;
            let errorMessage;
            let maxNumOfTries = 12; // Assuming a 5-second wait time, it gives a total of 1 minute. Adjust as needed.
            while (tries < maxNumOfTries) {
                try {
                    let message;
                    try {
                        message = await page.waitForSelector('#compiler-message');
                    } catch (messageTimeout) {
                        console.log('Loading Page taking too long. Retrying:', tries);
                    }

                    if (message) {
                        let currentStatus = await message.evaluate(node => node.innerText);
                        page.on('console', msg => {
                            for (let i = 0; i < msg.args().length; ++i)
                                console.log(`${i}: ${msg.args()[i]}`);
                        });
                        if (currentStatus === 'DONE') {
                            console.log('LWC Activated successfully');

                            // The below step is only required if Manged Package Runtime is disabled. Otherwise the above steps will generate a Custom LWC from OS
                            if(managedPackageRuntimeDisabled){
                                let omniscriptDesignerPageWrapper = siteUrl + '/lightning/cmp/' + packageNamespace + 'OmniDesignerAuraWrapper?c__recordId=' + eachOmniscriptId;
                                await page.goto(omniscriptDesignerPageWrapper);
                                await page.waitForSelector('section.tabContent.active.oneConsoleTab c-omni-designer-header lightning-button-menu button[part="button button-icon"]');
                                await page.waitForTimeout(2000); // This waits for 2 seconds. Adjust as needed.
                                await page.click('section.tabContent.active.oneConsoleTab c-omni-designer-header lightning-button-menu button[part="button button-icon"]');

                                let isMessageReceived = false;
                                // Expose a function to be called from the page context.
                                await page.exposeFunction('onMessageReceived', () => {
                                    isMessageReceived = true;
                                });

                                // Click on the "Deploy Standard Runtime Compatible LWC" option
                                await page.waitForSelector('section.tabContent.active.oneConsoleTab c-omni-designer-header a[role="menuitem"] > span.slds-truncate', { visible: true });
                                await page.waitForTimeout(2000); // This waits for 2 seconds. Adjust as needed.
                                let waitUntilMessageReceived = new Promise(async (resolve) => {
                                    await page.evaluate(() => {
                                        window.addEventListener('message', (event) => {
                                            if(event && event.data && event.data.key && event.data.status){
                                                if(event.data.status === 'DONE') {
                                                    window.onMessageReceived(); // Call the exposed function.
                                                }
                                                console.log('Status from omnilwccompile -> ' + event.data.status);
                                            }
                                        })

                                        const activeSection = document.querySelector('section.tabContent.active.oneConsoleTab');
                                        const menuItems = Array.from(activeSection.querySelectorAll('c-omni-designer-header a[role="menuitem"] > span.slds-truncate'));
                                        const targetItem = menuItems.find(item => item.textContent === "Deploy Standard Runtime Compatible LWC");
                                        targetItem && targetItem.click();
                                    });

                                    // Keep checking until the isMessageReceived flag becomes true.
                                    while (!isMessageReceived) {
                                        await page.waitForTimeout(1000); // Wait for 1 second before checking again.
                                    }
                                    resolve();
                                });
                                await waitUntilMessageReceived;
                            }
                            break;
                        } else if (currentStatus.startsWith('ERROR: No MODULE named markup')) {
                            errorMessage = 'Missing Custom LWC - ' + currentStatus;
                            break;
                        } else if (currentStatus.startsWith('ERROR')) {
                            errorMessage = 'Error Activating LWC - ' + currentStatus;
                            break;
                        }
                    }
                } catch (e) {
                    errorMessage = 'Error:' + e;
                }

                tries++;
                await page.waitForTimeout(5000); // This waits for 5 seconds. Adjust as needed.
            }

            if (tries === maxNumOfTries) {
                errorMessage = 'Activation took longer than expected - Aborting';
            }

            if (errorMessage) {
                console.log('LWC Activation Error:', errorMessage);
            }
        }


        //Loop in FlexCards and active
        if(flexCardsIds.length > 0) {
            let idsArrayString = flexCardsIds.join(',');
            let flexCardCompilePage = siteUrl + '/apex/' + packageNamespace + 'FlexCardCompilePage?id=' + idsArrayString;
            await page.goto(flexCardCompilePage, {timeout: loginTimeout});
            await page.waitForTimeout(5000);
            let tries = 0;
            const maxNumOfTries = 12; // Adjust based on your needs
            let errorMessage;

            while (tries < maxNumOfTries) {
                try {
                    const message = await page.waitForSelector('#compileMessage-0', { timeout: 5000 });

                    if (message) {
                        let currentStatus = await message.evaluate(node => node.innerText);
                        if (currentStatus === 'DONE SUCCESSFULLY') {
                            console.log('LWC Activated successfully');
                            break;
                        } else if (currentStatus === 'DONE WITH ERRORS') {
                            let jsonResulNode  = await page.waitForSelector('#resultJSON-0');
                            let jsonError = await jsonResulNode.evaluate(node => node.innerText);
                            console.log('LWC FlexCards Compilation Error Result:', jsonError);
                            break;
                        }
                    }
                } catch (e) {
                    console.log('Error Activating FlexCards:', e);
                }

                tries++;
                await page.waitForTimeout(5000);
            }

            if (tries === maxNumOfTries) {
                errorMessage = 'Activation took longer than expected - Aborted';
                process.exit(1); // Exit with a non-zero exit code to signal failure
            }
        }


    } catch (e) {
        try {
            await browser.close();
        } catch (e2) {
            console.log('Compelte1 '+e);
            process.exit(1); // Exit with a non-zero exit code to signal failure
        }
    }
}

// Call the function
try {
    compileOSAndFlexCards().then(r => {
        console.log('success');
        process.exit(0);
    }).catch(e => {
        console.log('Compelte2 '+e);
        process.exit(1); // Exit with a non-zero exit code to signal failure
    });
}  catch (error) {
    console.log('Compelte3 '+e);
    process.exit(1); // Exit with a non-zero exit code to signal failure
}
