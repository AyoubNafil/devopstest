trigger DeleteDestinationOrg on DestinationOrg__c (before delete) {
    for (DestinationOrg__c destOrg : Trigger.old) {
        if (destOrg.Destination__c == null || destOrg.Source__c == null) {
            // Check if either destination or source record is null
            // Query related records and delete if necessary
            List<Environment__c> relatedEnvironments = [SELECT Id FROM Environment__c WHERE Id IN :new Set<Id>{destOrg.Destination__c, destOrg.Source__c}];
            if (relatedEnvironments.size() < 2) {
                // If either destination or source doesn't exist, delete the destinationOrg record
                delete destOrg;
            }
        }
    }
}
