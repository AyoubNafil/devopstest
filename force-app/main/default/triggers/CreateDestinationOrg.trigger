trigger CreateDestinationOrg on DestinationOrg__c (before insert) {
    for (DestinationOrg__c record : Trigger.new) {
        // Perform validation checks here if necessary

        // Example: Prevent circular references
        // if (record.Destination__r.Id == record.Source__r.Id) {
        //     record.addError('Source cannot be the same as Destination.:'+record.Destination__r.Id+'-'+record.Source__r.Id);
        // }
        // Add more validation logic as required
    }
}
