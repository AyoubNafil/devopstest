trigger BranchingTrigger on Branching__c (before update) {
    // Get the record that is being updated
    Branching__c updatedRecord = Trigger.new[0];

    // Query for all records except the one being updated
    List<Branching__c> otherRecords = [SELECT Id, Assigned__c FROM Branching__c WHERE Id != :updatedRecord.Id];

    // If the record is being checked, uncheck all other records
    if (updatedRecord.Assigned__c) {
        for (Branching__c record : otherRecords) {
            record.Assigned__c = false;
        }
        // Update other records
        update otherRecords;
    }

    // If the record is being unchecked and no other record is checked, prevent unchecking it
    else {
        Boolean anyChecked = false;
        for (Branching__c record : otherRecords) {
            if (record.Assigned__c) {
                anyChecked = true;
                break;
            }
        }
        if (!anyChecked) {
            updatedRecord.addError('At least one record must be checked.');
        }
    }
}