import { LightningElement,track,wire } from 'lwc';
import PipelineManagerModal from 'c/pipelineManagerModal';

export default class PipelineManager extends LightningElement {
    
   

    renderedCallback() {
        this.initializeCanvas();
    }

    initializeCanvas() {
        const canvas = this.template.querySelector('canvas');
        const context = canvas.getContext('2d');

        const leftBoxes = this.template.querySelectorAll('.left-box');
        const rightBoxes = this.template.querySelectorAll('.right-box');

        console.log(leftBoxes);
        console.log(rightBoxes);
        if (leftBoxes.length < 1 || !rightBoxes) return;

        this.drawBoxes(context, leftBoxes, rightBoxes);

        const canvasRect = canvas.getBoundingClientRect();

        leftBoxes.forEach(box => {
            box.addEventListener('mousedown', (event) => {
                this.handleMouseDown(event, box, canvasRect);
            });

            box.addEventListener('mousemove', (event) => {
                this.handleMouseMove(event, box, canvasRect);
            });

            box.addEventListener('mouseup', () => {
                this.handleMouseUp();
            });
        });
    }

    drawBoxes(context, leftBoxes, rightBoxes) {
        context.clearRect(0, 0, context.canvas.width, context.canvas.height);
        context.strokeStyle = '#000';
        context.lineWidth = 2;
    
        leftBoxes.forEach((box, index) => {
            const startPos = this.getMiddlePosition(box);
            const endPos = this.getMiddlePosition(rightBoxes[0]);
            const midX = (startPos.x + endPos.x) / 2;
    
            // Draw first horizontal line
            context.beginPath();
            context.moveTo(startPos.x, startPos.y);
            context.lineTo(midX, startPos.y);
            context.stroke();
    
            // Draw vertical line
            context.beginPath();
            context.moveTo(midX, startPos.y);
            context.lineTo(midX, endPos.y);
            context.stroke();
    
            // Draw second horizontal line
            context.beginPath();
            context.moveTo(midX, endPos.y);
            context.lineTo(endPos.x, endPos.y);
            context.stroke();

            // Draw arrow
            this.arrow(context, { x: midX, y: endPos.y }, { x: endPos.x - rightBoxes[0].offsetWidth / 2 , y: endPos.y }, 5);
    
            
        });

        rightBoxes.forEach((box, index) => {
            if (index == rightBoxes.length-1) return;
            const startPos = this.getMiddlePosition(box);
            const endPos = this.getMiddlePosition(rightBoxes[index+1]);
            
    
            // Draw first horizontal line
            context.beginPath();
            context.moveTo(startPos.x, startPos.y);
            context.lineTo(endPos.x, endPos.y);
            context.stroke();

            // Draw arrow
            this.arrow(context, { x: startPos.x, y: startPos.y }, { x: endPos.x - rightBoxes[index+1].offsetWidth / 2 , y: endPos.y }, 5);
    
            
        });


       
        
    }
    
    arrow(ctx, p1, p2, size) {
        const angle = Math.atan2(p2.y - p1.y, p2.x - p1.x);
        const length = Math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2);
    
        ctx.save();
        ctx.translate(p1.x, p1.y);
        ctx.rotate(angle);
    
        // Draw arrow line
        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.lineTo(length - size, 0);
        ctx.stroke();
    
        // Draw arrowhead
        ctx.beginPath();
        ctx.moveTo(length - size, size);
        ctx.lineTo(length, 0);
        ctx.lineTo(length - size, -size);
        ctx.fill();
    
        ctx.restore();
    }

    getMiddlePosition(box) {
        return {
            x: box.offsetLeft + box.offsetWidth / 2,
            y: box.offsetTop + box.offsetHeight / 2
        };
    }

    handleMouseDown(event, box, canvasRect) {
        this.selectedBox = box;
        this.dragging = true;
        this.dragOffsetX = event.clientX - box.offsetLeft;
        this.dragOffsetY = event.clientY - box.offsetTop;
    }

    handleMouseMove(event, box, canvasRect) {
        if (this.dragging && this.selectedBox === box) {
            const mouseX = event.clientX - this.dragOffsetX;
            const mouseY = event.clientY - this.dragOffsetY;

            if (mouseX >= canvasRect.left && mouseX <= canvasRect.right - box.offsetWidth &&
                mouseY >= canvasRect.top && mouseY <= canvasRect.bottom - box.offsetHeight) {
                box.style.left = mouseX + 'px';
                box.style.top = mouseY + 'px';
                const canvas = this.template.querySelector('canvas');
                const context = canvas.getContext('2d');
                const leftBoxes = this.template.querySelectorAll('.left-box');
                const rightBoxes = this.template.querySelectorAll('.right-box');
                this.drawBoxes(context, leftBoxes, rightBoxes);
            }
        }
    }

    handleMouseUp() {
        this.dragging = false;
        this.selectedBox = null;
    }

    openModal() {
        PipelineManagerModal.open();
    }
}
