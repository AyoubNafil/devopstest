import { LightningElement,api } from 'lwc';
import openPullRequest from '@salesforce/apex/GitLabCommitController.openPullRequest';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class PullRequestAction extends LightningElement {

    @api recordId;

    @api invoke() {

        console.log(this.recordId);
        openPullRequest({ userStoryId: this.recordId })
        .then(result => {
            // Handle success
            console.log('Pull request opened successfully!:', result);
            const toastEvent = new ShowToastEvent({
                title: 'Success',
                message: 'Pull request opened successfully: '+result,
                variant: 'success'
            });
            this.dispatchEvent(toastEvent);
        })
        .catch(error => {
            // Handle error
            console.error('Failed to open pull request:', error.body.message);
            const toastEvent = new ShowToastEvent({
                title: 'Error',
                message: 'Failed to open pull request: ' + error.body.message,
                variant: 'error'
            });
            this.dispatchEvent(toastEvent);
        });

    }
}