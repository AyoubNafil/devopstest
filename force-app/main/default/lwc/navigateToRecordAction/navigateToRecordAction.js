import { LightningElement, api ,wire } from "lwc";
import { NavigationMixin } from "lightning/navigation";


export default class navigateToRecordAction extends NavigationMixin(LightningElement) {

  @api recordId;

  @api invoke() {
    //console.log(this.recordId);
    this[NavigationMixin.Navigate]({
      type: "standard__navItemPage",
      attributes: {
        apiName: 'MetadataGrid'
      },
      state: {
        //c__counter: '5',
        c__recId: this.recordId //must be string
      }
    });
  }

  
}