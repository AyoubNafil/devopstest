import { LightningElement,api } from 'lwc';
import commitSelectedFieldsUsId from '@salesforce/apex/GitLabCommitController.commitSelectedFieldsUsId';
import commitSelectedApexClassesUsId from '@salesforce/apex/GitLabCommitController.commitSelectedApexClassesUsId';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class CommitAction extends LightningElement {

    @api recordId;

    @api async invoke() {

        console.log(this.recordId);
        // await commitSelectedFieldsUsId({ userStoryId: this.recordId })
        // .then(result => {
        //     // Handle success
        //     console.log('GitLab Commit Successful:', result);
        //     const toastEvent = new ShowToastEvent({
        //         title: 'Success',
        //         message: 'GitLab Commit Successful: '+result,
        //         variant: 'success'
        //     });
        //     this.dispatchEvent(toastEvent);
        // })
        // .catch(error => {
        //     // Handle error
        //     console.error('GitLab Commit Error:', error.body.message);
        //     const toastEvent = new ShowToastEvent({
        //         title: 'Error',
        //         message: 'GitLab Commit Error: ' + error.body.message,
        //         variant: 'error'
        //     });
        //     this.dispatchEvent(toastEvent);
        // });


        await commitSelectedApexClassesUsId({ userStoryId: this.recordId })
        .then(result => {
            // Handle success
            console.log('GitLab Commit Successful:', result);
            const toastEvent = new ShowToastEvent({
                title: 'Success',
                message: 'GitLab Commit Successful: '+result,
                variant: 'success'
            });
            this.dispatchEvent(toastEvent);
        })
        .catch(error => {
            // Handle error
            console.error('GitLab Commit Error:', error.body.message);
            const toastEvent = new ShowToastEvent({
                title: 'Error',
                message: 'GitLab Commit Error: ' + error.body.message,
                variant: 'error'
            });
            this.dispatchEvent(toastEvent);
        });

    }

}