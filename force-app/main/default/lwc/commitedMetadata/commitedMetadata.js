import { LightningElement,track,api,wire } from 'lwc';
import GetUserStoryMetadataSObject from '@salesforce/apex/UserStoriesController.GetUserStoryMetadataSObject';
import GetUserStoryMetadataAC from '@salesforce/apex/UserStoriesController.GetUserStoryMetadataAC';


export default class CommitedMetadata extends LightningElement {

    @api recordId;
    @track selectedObjectFields = [];
    @track selectedObjectFields2 = [];

    @track selectedApexClasses = [];
    @track selectedApexClasses2 = [];
   

    columns = [
        { label: 'Object Name', fieldName: 'Object_Name__c' },
        { label: 'Field Name', fieldName: 'Field_Name__c' },
        { label: 'Field API Name', fieldName: 'Field_API_Name__c' },
        { label: 'Field Type', fieldName: 'Field_Type__c' }
    ];

    columns2 = [
        { label: 'Class Name', fieldName: 'Class_Name__c' }
        
    ];

    @wire(GetUserStoryMetadataSObject ,{ UsId: '$recordId' })
    wiredMetadata({ error, data }) {

        console.log(this.recordId);

        if (data) {
            //this.selectedObjectFields = data;
             // Initialize an empty object to store the transformed data
        let transformedData = {};

        // Iterate over the data array
        data.forEach(item => {
            // Extract the object name
            let objectName = item.Object_Name__c;

            // If the object name doesn't exist in the transformedData object, initialize it
            if (!transformedData[objectName]) {
                transformedData[objectName] = {
                    Object_Name__c: item.Object_Name__c,
                    _children: []
                };
            }

            // Add the field data to the _children array of the respective object name
            transformedData[objectName]._children.push({
                Object_Name__c: item.Object_Name__c,
                Field_Name__c: item.Field_Name__c,
                Field_API_Name__c: item.Field_API_Name__c,
                Field_Type__c: item.Field_Type__c,
                toDelete__c: item.toDelete__c
            });
        });

        // Convert the transformedData object to an array
        //this.selectedObjectFields = Object.values(transformedData);
        this.selectedObjectFields = Object.values(transformedData)
                .map(object => ({
                    ...object,
                    _children: object._children.filter(field => !field.toDelete__c)
                }))
                .filter(object => object._children.length > 0);

        this.selectedObjectFields2 = Object.values(transformedData)
                .map(object => ({
                    ...object,
                    _children: object._children.filter(field => field.toDelete__c)
                }))
                .filter(object => object._children.length > 0);

        console.log("this.selectedObjectFields: "+JSON.stringify(this.selectedObjectFields));
        console.log("this.selectedObjectFields2: "+JSON.stringify(this.selectedObjectFields2));
           
            
        } else if (error) {
            console.error('Error retrieving : ', error);
        }
    }


    @wire(GetUserStoryMetadataAC ,{ UsId: '$recordId' })
    wiredMetadata2({ error, data }) {

        console.log(this.recordId);

        if (data) {


            this.selectedApexClasses = data.filter(field => !field.toDelete__c);
            this.selectedApexClasses2 = data.filter(field => field.toDelete__c);
    
            
        } else if (error) {
            console.error('Error retrieving : ', error);
        }
    }




    

    handleRowSelection(event) {
        this.selectedRows = event.detail.selectedRows;
        // Handle selected rows as needed
        console.log('Selected Rows:', JSON.stringify(this.selectedRows));
    }

    handleRowSelection2(event) {
        this.selectedRows2 = event.detail.selectedRows;
        // Handle selected rows as needed
        console.log('Selected Rows:', JSON.stringify(this.selectedRows2));
    }



    
    
    
}