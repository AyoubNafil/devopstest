import { track , wire } from "lwc";
import getEnvironments from '@salesforce/apex/EnvironmentController.getEnvironments';
import createDestinationOrgRecord from '@salesforce/apex/DestinationOrgController.createDestinationOrgRecord';

import LightningModal from 'lightning/modal';

export default class PipelineManagerModal extends LightningModal  {

	@track selectedDestination;
    @track selectedSource;
    @track destinationOptions = [];
    @track sourceOptions = [];

	@wire(getEnvironments)
    wiredEnvironments({ error, data }) {
        if (data) {
            // Populate destinationOptions and sourceOptions with retrieved data
            // Format the data into { label: 'Label', value: 'Id' } format
            this.destinationOptions = data.map(env => ({ label: env.Name, value: env.Id }));
            this.sourceOptions = data.map(env => ({ label: env.Name, value: env.Id }));
        } else if (error) {
            console.error('Error fetching environments:', error);
            // Handle error
        }
    }

    // Handle destination combobox change
    handleDestinationChange(event) {
        this.selectedDestination = event.detail.value;
        // Remove selected destination from source combobox options
        this.sourceOptions = this.sourceOptions.filter(option => option.value !== this.selectedDestination);
    }

    // Handle source combobox change
    handleSourceChange(event) {
        this.selectedSource = event.detail.value;
        // Remove selected source from destination combobox options
        this.destinationOptions = this.destinationOptions.filter(option => option.value !== this.selectedSource);
    }

    // Handle submit button click
    async save() {
        // Validate selected destination and source
        if (!this.selectedDestination || !this.selectedSource) {
            // Show error message if destination or source is not selected
            // You can implement your error handling logic here
            return;
        }

        // Call Apex method to create DestinationOrg__c record
        await createDestinationOrgRecord({
            destinationId: this.selectedDestination,
            sourceId: this.selectedSource
        })
        .then(result => {
            // Handle success response
            // You can display success message or perform any other action
            console.log('DestinationOrg__c record created successfully');
            // Close the modal
            this.closeModal();
        })
        .catch(error => {
            // Handle error response
            // You can display error message or perform any other action
            console.error('Error creating DestinationOrg__c record:', error);
        });
    }

	closeModal() {
        this.close();
    }

    // save() {
    //     console.log('We will save the data and then close modal');
    // }


}