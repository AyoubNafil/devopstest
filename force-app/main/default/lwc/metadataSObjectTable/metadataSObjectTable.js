import { LightningElement, wire, track } from 'lwc';
import getCustomObjectFields from '@salesforce/apex/GetSObjectWithFields.getCustomObjectFields';
//import commitSelectedFields from '@salesforce/apex/GitLabCommitController.commitSelectedFields';
//import SObjectToXml from '@salesforce/apex/GitLabCommitController.SObjectToXml';
//import generateCustomObjectMetadataXml from '@salesforce/apex/GitLabCommitController.generateCustomObjectMetadataXml';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {CurrentPageReference} from 'lightning/navigation';
import GetUserStory from '@salesforce/apex/UserStoriesController.GetUserStory';
import insertUserStoryMetadata from '@salesforce/apex/UserStoriesController.insertUserStoryMetadata';

import getAllApexClasses from '@salesforce/apex/GitLabCommitController.getAllApexClasses';


export default class MetadataSObjectTable extends LightningElement {

    columnsSO = [
        { label: 'Object Name', fieldName: 'objectName' },
        { label: 'Field Name', fieldName: 'fieldName' },
        { label: 'Field API Name', fieldName: 'fieldAPIName' },
        { label: 'Field Type', fieldName: 'fieldType' }
    ];

    columnsAP = [
        { label: 'Class Name', fieldName: 'Name' },
        { label: 'IsValid', fieldName: 'IsValid' },
        { label: 'Status', fieldName: 'Status' },
    ];

    UserStoryID;
    @track UserStory;
    @track UserStoryName;
    @track OperationName;

    @track selectedRows;
    @track selectedObject;
    @track selectedObjectType = 'all';
    @track listField = [];
    @track selectedObjectFields = [];
    @track uniqueObjectNames = [];
    //@track isCustom = false;
    @track objectOptions = [];

    @track selectedRows2;
    //@track selectedApexClasses = [];
    @track apexClasses = [];


    @track showSO;
    @track showAC;


    @wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
        if (currentPageReference) {
            this.UserStoryID = currentPageReference.state.c__recId;
            console.log(currentPageReference.state.c__recId);
        }
    }

    @wire(GetUserStory ,{ UsId: '$UserStoryID' })
    getUserStoryParameters({ error, data }) {

        console.log(this.UserStoryID);

        if (data) {
            this.UserStory = data.map((obj) => ({ label: obj.Name, value: obj.Id }));
            this.UserStoryName = this.UserStory[0].value;
            console.log("value:  "+this.UserStoryName);
            console.log(JSON.stringify(this.UserStory));
           
            
        } else if (error) {
            console.error('Error retrieving cases: ', error);
        }
    }
 

    


    @wire(getAllApexClasses)
    wiredApexClasses({ data, error }) {
        if (data) {
            //console.log(data);
            this.apexClasses = data;
            console.log('apexClasses:', JSON.stringify(this.apexClasses));

           
            // this.objectOptions = this.listField
            //     .map((obj) => ({ label: obj, value: obj }))
            //     .sort((a, b) => a.label.localeCompare(b.label));
        } else if (error) {
            console.error('Error retrieving fields: ', error);
        }
    }


    

    @wire(getCustomObjectFields)
    wiredFields({ data, error }) {
        if (data) {
            //console.log(data);
            this.listField = data;
            this.uniqueObjectNames = [...new Set(this.listField.map((field) => field.objectName))];
            this.objectOptions = this.uniqueObjectNames
                .map((obj) => ({ label: obj, value: obj }))
                .sort((a, b) => a.label.localeCompare(b.label));
        } else if (error) {
            console.error('Error retrieving fields: ', error);
        }
    }

    get objectTypeOptions() {
        return [
            { label: 'All', value: 'all' },
            { label: 'Standard', value: 'standard' },
            { label: 'Custom', value: 'custom' },
        ];
    }

    get operationTypeOptions() {
        return [
            { label: 'Commit Files', value: 'CF' },
            { label: 'Destructive Changes', value: 'DC' },
            { label: 'Full Profiles and Permission Sets', value: 'FPPS' }
          
        ];
    }

    get metadataTypeOptions() {
        return [
            { label: 'Apex Classes', value: 'AC' },
            { label: 'SObjects', value: 'SO' },
            
          
        ];
    }

    handleObjectTypeChange(event) {
        this.selectedObjectType = event.detail.value;
    }

    handleOperationTypeChange(event) {
        this.OperationName = event.detail.value;
    }

    get filteredObjectOptions() {
        // const searchTerm = this.selectedObject ? this.selectedObject.toLowerCase() : '';
        // const filteredObjects = this.objectOptions.filter(
        //     (obj) => obj.label.toLowerCase().includes(searchTerm)
        // );
        const filteredObjects = this.objectOptions;
        if (this.selectedObjectType === 'custom') {
            return filteredObjects.filter((obj) => obj.label.endsWith('__c'));
        } else if (this.selectedObjectType === 'standard') {
            return filteredObjects.filter((obj) => !obj.label.endsWith('__c'));
        } else {
            return filteredObjects;
        }
    }

    handleObjectChange(event) {
        this.selectedObject = event.detail.value;
    }


    handleShowFields() {
        this.selectedObjectFields = this.listField.filter(
            (field) => field.objectName === this.selectedObject
        );
    }



    handleRowSelection(event) {
        this.selectedRows = event.detail.selectedRows;
        // Handle selected rows as needed
        console.log('Selected Rows:', JSON.stringify(this.selectedRows));
    }
    handleRowSelection2(event) {
        this.selectedRows2 = event.detail.selectedRows;
        // Handle selected rows as needed
        console.log('Selected Rows:', JSON.stringify(this.selectedRows2));
    }

    handleMetadataTypeChange(event){
        if (event.detail.value === 'SO') {
            this.showSO = true;
            this.showAC = false;
        } else {
            this.showSO = false;
            this.showAC = true;
        }
    }


    async commitSOToUs() {
        if (this.selectedRows && this.selectedRows.length > 0) {
            const toDelete = this.OperationName === 'DC'; // Set isDelete based on the OperationName value
            const metadataList = this.selectedRows.map(row => {
                return {
                    Name: row.fieldAPIName,
                    Field_API_Name__c: row.fieldAPIName,
                    Field_Name__c: row.fieldName,
                    Field_Type__c: row.fieldType,
                    Object_Name__c: row.objectName,
                    User_Story__c: this.UserStoryID,
                    toDelete__c: toDelete, // Set isDelete based on the OperationName value
                    Type__c:'SO'
                };
            });

            console.log(JSON.stringify(metadataList));

            await insertUserStoryMetadata({ metadataList: metadataList })
                .then(result => {
                    console.log('User Story Metadata inserted successfully:', result);
                    // Handle success if needed
                    const toastEvent = new ShowToastEvent({
                                            title: 'Success',
                                            message: 'Commit Successful',
                                            variant: 'success'
                                        });
                    this.dispatchEvent(toastEvent);
                })
                .catch(error => {
                    console.error('Error inserting User Story Metadata:', error);
                    // Handle error if needed
                    const toastEvent = new ShowToastEvent({
                                            title: 'Error',
                                            message: 'Commit Error',
                                            variant: 'error'
                                        });
                    this.dispatchEvent(toastEvent);
                });
        } else {
            console.warn('No rows selected.');
        }
    }

    async commitACToUs() {
        if (this.selectedRows2 && this.selectedRows2.length > 0) {
            const toDelete = this.OperationName === 'DC'; // Set isDelete based on the OperationName value
            const metadataList = this.selectedRows2.map(row => {
                return {
                    Name: row.Name,
                    Class_Name__c: row.Name,
                    User_Story__c: this.UserStoryID,
                    toDelete__c: toDelete, // Set isDelete based on the OperationName value
                    Type__c:'AC'
                };
            });

            console.log(JSON.stringify(metadataList));

            await insertUserStoryMetadata({ metadataList: metadataList })
                .then(result => {
                    console.log('User Story Metadata inserted successfully:', result);
                    // Handle success if needed
                    const toastEvent = new ShowToastEvent({
                                            title: 'Success',
                                            message: 'Commit Successful',
                                            variant: 'success'
                                        });
                    this.dispatchEvent(toastEvent);
                })
                .catch(error => {
                    console.error('Error inserting User Story Metadata:', error);
                    // Handle error if needed
                    const toastEvent = new ShowToastEvent({
                                            title: 'Error',
                                            message: 'Commit Error',
                                            variant: 'error'
                                        });
                    this.dispatchEvent(toastEvent);
                });
        } else {
            console.warn('No rows selected.');
        }
    }


    

    // commitToGitLab() {
    //         //console.log(JSON.stringify(this.selectedRows));
    //         const selectedFieldAPINames = this.selectedRows.map(row => row.objectName);
    //         console.log('Selected Field API Names:', JSON.stringify(selectedFieldAPINames));



    //         commitSelectedFields({ fields: selectedFieldAPINames  })
    //             .then(result => {
    //                 // Handle success, if needed
    //                 console.log('GitLab Commit Successful:', result);
    //             })
    //             .catch(error => {
    //                 // Handle errors appropriately
    //                 console.error('GitLab Commit Error:', error.body.message);
    //             });

    //         // SObjectToXml({ fields: selectedFieldAPINames[0]  })
    //         //     .then(result => {
    //         //         // Handle success, if needed
    //         //         console.log('Successful:', result);
    //         //     })
    //         //     .catch(error => {
    //         //         // Handle errors appropriately
    //         //         console.error('Error:', error.body.message);
    //         //     });


    //         // generateCustomObjectMetadataXml({ objectApiName: selectedFieldAPINames[0]  })
    //         //     .then(result => {
    //         //         // Handle success, if needed
    //         //         console.log('Successful:', result);
    //         //     })
    //         //     .catch(error => {
    //         //         // Handle errors appropriately
    //         //         console.error('Error:', error.body.message);
    //         //     });


    //     }

    ////////////////////////////////////////Working One//////////////////////////////////////
    // commitToGitLab() {
    //     // Check if any rows are selected
    //     if (this.selectedRows && this.selectedRows.length > 0) {
    //         // Convert selected rows to ObjectMetadata objects
    //         const objectMetadataList = [];

    //         this.selectedRows.forEach(row => {
    //             const existingMetadata = objectMetadataList.find(metadata => metadata.objectName === row.objectName);
    //             if (existingMetadata) {
    //                 existingMetadata.fieldApiNames.push(row.fieldAPIName);
    //             } else {
    //                 objectMetadataList.push({
    //                     objectName: row.objectName,
    //                     fieldApiNames: [row.fieldAPIName]
    //                 });
    //             }
    //         });

    //         console.log(JSON.stringify(objectMetadataList));

    //         // Call Apex method to commit selected fields
    //         commitSelectedFields({ objectMetadataListJson: JSON.stringify(objectMetadataList) })
    //             .then(result => {
    //                 // Handle success
    //                 console.log('GitLab Commit Successful:', result);
    //                 const toastEvent = new ShowToastEvent({
    //                     title: 'Success',
    //                     message: 'GitLab Commit Successful: '+result,
    //                     variant: 'success'
    //                 });
    //                 this.dispatchEvent(toastEvent);
    //             })
    //             .catch(error => {
    //                 // Handle error
    //                 console.error('GitLab Commit Error:', error.body.message);
    //                 const toastEvent = new ShowToastEvent({
    //                     title: 'Error',
    //                     message: 'GitLab Commit Error: ' + error.body.message,
    //                     variant: 'error'
    //                 });
    //                 this.dispatchEvent(toastEvent);
    //             });
    //     } else {
    //         console.warn('No rows selected.');
    //     }
    // }

    
    
}