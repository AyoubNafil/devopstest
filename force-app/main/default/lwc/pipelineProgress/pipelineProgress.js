import { LightningElement, wire, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getPipelineStages from '@salesforce/apex/PipelinesController.getPipelineStages';

export default class PipelineProgress extends NavigationMixin(LightningElement) {
    @api recordId;
    @track stages;

    @wire(getPipelineStages, { PipId: '$recordId' })
    wiredStages({ error, data }) {
        if (data) {
            this.stages = data.map(stage => ({
                ...stage,
                succeeded: stage.Build_Status__c === 'success',
                failed: stage.Build_Status__c === 'failed'
            }));
        } else if (error) {
            console.error('Error retrieving stages: ', error);
        }
    }

    handleStageClick(event) {
        const stageId = event.currentTarget.dataset.stageId;
        this.navigateToStageRecord(stageId);
    }

    navigateToStageRecord(stageId) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: stageId,
                objectApiName: 'Pipeline__c', // Replace with your object API name
                actionName: 'view'
            }
        });
    }
}