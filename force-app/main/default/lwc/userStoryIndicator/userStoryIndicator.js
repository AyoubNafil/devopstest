import { LightningElement,api,wire,track } from 'lwc';
import GetUserStory from '@salesforce/apex/UserStoriesController.GetUserStory';

export default class UserStoryIndicator extends LightningElement {

    @api recordId;

    @track step;

    steps = [
        { label: 'US Branch', value: 'USB' },
        { label: 'pocB2C', value: 'test' },
        { label: 'UAT', value: 'uat' },
        { label: 'PRODUCTION', value: 'master' }
    ];

    @wire(GetUserStory ,{ UsId: '$recordId' })
    getUserStory({ error, data }) {
        console.log("ttttttttttttt");
        if (data) {
            console.log(JSON.stringify(data));
            this.step = data[0].Step__c;
            console.log(this.step);
            
        } else if (error) {
            console.error('Error retrieving task step: ', error);
        }
    }
}