@isTest
private class GitLabCommitControllerTest {

    @isTest
    static void testCommitSelectedFieldsUsId() {
        // Set up test data
        User_Story__c userStory = new User_Story__c(Name = 'Test User Story');
        insert userStory;

        User_Story_Metadata__c metadata1 = new User_Story_Metadata__c(
            Name = 'Test Metadata 1',
            Field_API_Name__c = 'Build_Status__c',
            Field_Name__c = 'Build Status',
            Field_Type__c = 'Text',
            Object_Name__c = 'Stage__c',
            User_Story__c = userStory.Id
        );
        User_Story_Metadata__c metadata2 = new User_Story_Metadata__c(
            Name = 'Test Metadata 2',
            Field_API_Name__c = 'Finished_At__c',
            Field_Name__c = 'Finished At',
            Field_Type__c = 'DateTime',
            Object_Name__c = 'Stage__c',
            User_Story__c = userStory.Id
        );
        insert new List<User_Story_Metadata__c>{metadata1, metadata2};

        // Set up mock HTTP response
        Test.setMock(HttpCalloutMock.class, new GitLabCommitControllerTest.MockHttpResponse());

        // Call the method to be tested
        Test.startTest();
        String result = GitLabCommitController.commitSelectedFieldsUsId(userStory.Id);
        Test.stopTest();

        // Verify the result
        System.assertEquals('Commit successful!', result, 'Expected successful commit message');
    }

    // Mock HTTP response class
    private class MockHttpResponse implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(201);
            res.setBody('{"message": "Commit successful!"}');
            return res;
        }
    }
}