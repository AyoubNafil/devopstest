@RestResource(urlMapping='/api/Webhooks/mergeRequestDetails/*')
global without sharing class webhookSubscriptionMergeRequest {

    private static final String EXPECTED_GITLAB_TOKEN = 'webhooksecret';

    private class User {
        String id;
        String name;
        String username;
        String avatar_url;
        String email;
    }

    private class Project {
        String id;
        String name;
        String description;
        String web_url;
        String avatar_url;
        String git_ssh_url;
        String git_http_url;
        String namespace;
        Integer visibility_level;
        String path_with_namespace;
        String default_branch;
        String ci_config_path;
        String homepage;
        String url;
        String ssh_url;
        String http_url;
    }

    private class MergeRequestAttributes {
        String id;
        Integer iid;
        String target_branch;
        String source_branch;
        Integer source_project_id;
        Integer author_id;
        List<Integer> assignee_ids;
        Integer assignee_id;
        List<Integer> reviewer_ids;
        String title;
        String created_at;
        String updated_at;
        String last_edited_at;
        Integer last_edited_by_id;
        Integer milestone_id;
        Integer state_id;
        String state;
        Boolean blocking_discussions_resolved;
        Boolean work_in_progress;
        Boolean draft;
        Boolean first_contribution;
        String merge_status;
        Integer target_project_id;
        String description;
        Integer total_time_spent;
        Integer time_change;
        String human_total_time_spent;
        String human_time_change;
        String human_time_estimate;
        String url;
        String action;
    }

    private class Changes {
        UpdatedById updatedById;
        Draft draft;
        UpdatedAt updatedAt;
        Labels labels;
        LastEditedAt lastEditedAt;
        LastEditedById lastEditedById;
    }


    private class UpdatedById {
        String previous;
        String current;
    }
    private class Draft {
        Boolean previous;
        Boolean current;
    }
    // private class UpdatedAt {
    //     String previous;
    //     String current;
    // }

    public class UpdatedAt {
        String previous;
        String current;
    }
    
    private class Labels {
        List<Label> previous;
        List<Label> current;
    }
    private class LastEditedAt {
        String previous;
        String current;
    }
    private class LastEditedById {
        String previous;
        String current;
    }

    private class Label {
        Integer id;
        String title;
        String color;
        Integer project_id;
        String created_at;
        String updated_at;
        Boolean template;
        String description;
        String type;
        Integer group_id;
    }

    private class LastCommit {
        String id;
        String message;
        String title;
        String timestamp;
        String url;
        Author author;
    }

    private class Author {
        String name;
        String email;
    }

    private class MergeRequest {
        String object_kind;
        String event_type;
        User user;
        Project project;
        MergeRequestAttributes object_attributes;
        Project source;
        Project target;
        LastCommit last_commit;
        List<Label> labels;
        Changes changes;
        List<User> assignees;
        List<User> reviewers;
    }

    @HttpPost
    global static String handleMergeRequestNotification() {
        String responseMessage;

        try {
            // Check the X-Gitlab-Token header
            String gitlabToken = RestContext.request.headers.get('X-Gitlab-Token');
            
            if (gitlabToken == null || !gitlabToken.equals(EXPECTED_GITLAB_TOKEN)) {
                throw new MergeRequestWebhookException('Invalid Gitlab Token');
            }

            String requestBody = RestContext.request.requestBody.toString();
            MergeRequest mergeRequest = (MergeRequest) JSON.deserialize(requestBody, MergeRequest.class);

            // Process the merge request details as needed

            // Extract user story name from the source branch
            String sourceBranch = mergeRequest.object_attributes.source_branch;
            String userStoryName = getUserStoryNameFromBranch(sourceBranch);

            // Process the merge request details as needed
            if (mergeRequest.object_attributes.action.equalsIgnoreCase('approved')) {
                // If the merge request is merged, update the related User_story__c record
                User_story__c userStory = [SELECT Id,Name, Step__c FROM User_story__c WHERE Name = :userStoryName];
                if (userStory != null) {
                    userStory.Step__c = 'test'; // Update Status__c field to 'test'
                    update userStory;
                }
            }

            PullRequest__c	PullRequestRecord = new PullRequest__c();
            PullRequestRecord.Gitlab_Id__c = String.valueOf(mergeRequest.object_attributes.id);
            PullRequestRecord.Name = mergeRequest.object_attributes.title;
            PullRequestRecord.description__c = mergeRequest.object_attributes.description;
            PullRequestRecord.source_branch__c = mergeRequest.object_attributes.source_branch;
            PullRequestRecord.target_branch__c = mergeRequest.object_attributes.target_branch;
            //PullRequestRecord.assignees_username__c = mergeRequest.assignees[0].username;
            //PullRequestRecord.reviewers_username__c = mergeRequest.reviewers[0].username;
            PullRequestRecord.merge_status__c = mergeRequest.object_attributes.merge_status;

            
            Database.upsert(PullRequestRecord, PullRequest__c.Fields.Gitlab_Id__c, false);

            responseMessage = 'Merge request processed successfully';

        } catch (MergeRequestWebhookException mwe) {
            responseMessage = 'Error: ' + mwe.getMessage();
        } catch (Exception e) {
            responseMessage = 'Error: ' + e.getMessage();
        }

        return responseMessage;
    }

    private class MergeRequestWebhookException extends Exception {

    }

    // Helper method to extract user story name from the branch name
    private static String getUserStoryNameFromBranch(String sourceBranch) {
        // Example implementation assuming the branch name format is "feature/UserStoryName"
        String[] parts = sourceBranch.split('/');
        if (parts.size() > 1) {
            return parts[1];
        } else {
            return null;
        }
    }
}