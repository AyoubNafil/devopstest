@isTest
private class webhookSubscriptionPipelineTest {

    @isTest
    static void testHandlePipelineNotification_ValidToken() {
        // Set up test data
        String requestBody = '{"object_kind":"pipeline","object_attributes":{"id":"123","status":"success","detailed_status":"Pipeline succeeded","stages":["build","test"],"created_at":"2024-01-24T12:00:00Z","finished_at":"2024-01-24T12:10:00Z","duration":600,"queued_duration":30,"url":"https://example.com/pipeline/123"},"merge_request":null,"user":null,"project":{"name":"Test Project"},"commit_x":{"title":"Test Commit"},"builds":[{"id":"1","stage":"build","name":"Build Job","status":"success","created_at":"2024-01-24T12:00:00Z","started_at":"2024-01-24T12:01:00Z","finished_at":"2024-01-24T12:05:00Z","duration":240,"queued_duration":10},{"id":"2","stage":"test","name":"Test Job","status":"success","created_at":"2024-01-24T12:06:00Z","started_at":"2024-01-24T12:07:00Z","finished_at":"2024-01-24T12:10:00Z","duration":180,"queued_duration":20}]}';
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/api/Webhooks/pipelineDetails';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.addHeader('X-Gitlab-Token', 'webhooksecret');
        request.requestBody = Blob.valueOf(requestBody);
        
        RestContext.request = request;

        // Call the method to be tested
        Test.startTest();
        String responseMessage = webhookSubscriptionPipeline.handlePipelineNotification();
        Test.stopTest();

        // Verify the response message
        System.assertEquals('Pipeline records processed successfully', responseMessage, 'Expected successful pipeline processing');
        
        // Verify that the Pipeline__c record is created
        List<Pipeline__c> pipelineList = [SELECT Id FROM Pipeline__c];
        System.assertEquals(1, pipelineList.size(), 'Expected one Pipeline__c record to be created');
        Pipeline__c pipeline = pipelineList[0];
        System.assertEquals('123', pipeline.Gitlab_Id__c, 'Expected Gitlab ID to match');
        // Verify other properties as well

        // Verify that the Stage__c records are created
        List<Stage__c> stageList = [SELECT Id FROM Stage__c];
        System.assertEquals(2, stageList.size(), 'Expected two Stage__c records to be created');
        // Verify properties of Stage__c records
    }

    @isTest
    static void testHandlePipelineNotification_InvalidToken() {
        // Set up invalid token
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/api/Webhooks/pipelineDetails';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.addHeader('X-Gitlab-Token', 'invalidtoken');
        request.requestBody = Blob.valueOf('{}');

        RestContext.request = request;

        // Call the method to be tested
        Test.startTest();
        String responseMessage = webhookSubscriptionPipeline.handlePipelineNotification();
        Test.stopTest();

        // Verify the response message for invalid token
        System.assertEquals('Error: Invalid Gitlab Token', responseMessage, 'Expected error message for invalid token');
    }
}