@isTest
private class BranchingTriggerHandlerTest {
    @isTest
    static void testOneRecordChecked() {
        // Create test data
        Branching__c branch1 = new Branching__c(Assigned__c = false);
        Branching__c branch2 = new Branching__c(Assigned__c = true);
        Branching__c branch3 = new Branching__c(Assigned__c = false);
        insert new List<Branching__c>{ branch1, branch2, branch3 };

        // Update a record to be checked
        branch1.Assigned__c = true;
        update branch1;

        // Verify only one record is checked
        List<Branching__c> checkedRecords = [SELECT Id FROM Branching__c WHERE Assigned__c = true];
        System.assertEquals(1, checkedRecords.size(), 'Expected only one record to be checked');
        System.assertEquals(branch1.Id, checkedRecords[0].Id, 'Expected correct record to be checked');

        // Verify other records are unchecked
        List<Branching__c> uncheckedRecords = [SELECT Id FROM Branching__c WHERE Id != :branch1.Id];
        for (Branching__c record : uncheckedRecords) {
            System.assertEquals(false, record.Assigned__c, 'Expected record to be unchecked');
        }
    }
}