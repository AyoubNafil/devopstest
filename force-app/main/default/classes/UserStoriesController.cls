public class UserStoriesController {

    @AuraEnabled(cacheable=true)
    public static List<User_Story__c>  GetUserStory(String UsId) {
        return [SELECT Id, Name,Step__c FROM User_Story__c where Id=: UsId] ;

    }
    

    @AuraEnabled
    public static void insertUserStoryMetadata(List<User_Story_Metadata__c> metadataList) {
        insert metadataList;
    }


    // @AuraEnabled(cacheable=true)
    // public static List<User_Story_Metadata__c>  GetUserStoryMetadataSObject(String UsId) {
    //     return [SELECT Id,Name,Field_API_Name__c, Field_Name__c, Field_Type__c, Object_Name__c, User_Story__c,toDelete__c FROM User_Story_Metadata__c where User_Story__c=: UsId] ;

    // }

    @AuraEnabled(cacheable=true)
    public static List<User_Story_Metadata__c>  GetUserStoryMetadataSObject(String UsId) {
        return [SELECT Id,Name,Field_API_Name__c, Field_Name__c, Field_Type__c, Object_Name__c, User_Story__c,toDelete__c FROM User_Story_Metadata__c where User_Story__c=: UsId AND Type__c = 'SO'] ;

    }

    @AuraEnabled(cacheable=true)
    public static List<User_Story_Metadata__c>  GetUserStoryMetadataAC(String UsId) {
        return [SELECT Id,Name,Class_Name__c, User_Story__c,toDelete__c FROM User_Story_Metadata__c where User_Story__c=: UsId AND Type__c = 'AC' ] ;

    }



}