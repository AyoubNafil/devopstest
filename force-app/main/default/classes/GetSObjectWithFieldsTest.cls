@isTest
private class GetSObjectWithFieldsTest {

    @isTest
    static void testGetCustomObjectFields() {
        // Create test data or mock schema describe calls as needed
        Test.startTest();
        // Call the method to be tested
        List<GetSObjectWithFields.ObjectFieldWrapper> objectFields = GetSObjectWithFields.getCustomObjectFields();
        // Perform assertions on the returned data
        System.assertNotEquals(null, objectFields, 'Object fields should not be null');
        // Add more assertions as needed based on expected behavior
        Test.stopTest();
    }
}