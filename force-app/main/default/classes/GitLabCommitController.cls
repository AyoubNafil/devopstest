// GitLabCommitController.cls
public class GitLabCommitController {

   

    public class ObjectMetadata {
        @AuraEnabled
        public String objectName;
        @AuraEnabled
        public List<String> fieldApiNames;
    }


    private static String getSessionId()
	{
		//Workaround because Lightning Component sessions are not API enabled
		//https://salesforce.stackexchange.com/questions/110515/getting-session-id-in-lightning
		return Test.isRunningTest() ? UserInfo.getSessionId() : Page.SessionId.getContent().toString();
	}

   
    @AuraEnabled(cacheable=true)
    public static String generateCustomObjectMetadataXml(String objectApiName) {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        
        
        service.SessionHeader.sessionId = getSessionId();
        // Specify the type of metadata you want to retrieve (in this case, CustomObject)
        MetadataService.CustomObject customObject = 
            (MetadataService.CustomObject) service.readMetadata('CustomObject', new String[] { objectApiName }).getRecords()[0];

        // Now, you can construct the XML string using the obtained metadata
        String xml = '<?xml version="1.0" encoding="UTF-8"?>\n' +
            '<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">\n' +
            '    <actionOverrides>\n';

        for (MetadataService.ActionOverride actionOverride : customObject.actionOverrides) {
            xml += '        <actionName>' + actionOverride.actionName + '</actionName>\n' +
                   '        <type>' + actionOverride.type_x + '</type>\n';
        }

        // Add other fields as needed

        xml += '    </actionOverrides>\n' +
            '    <allowInChatterGroups>' + customObject.allowInChatterGroups + '</allowInChatterGroups>\n' +
            '    <compactLayoutAssignment>' + customObject.compactLayoutAssignment + '</compactLayoutAssignment>\n' +
            '    <deploymentStatus>' + customObject.deploymentStatus + '</deploymentStatus>\n' +
            // Add other fields
            '</CustomObject>';

        return xml;
    }



    @AuraEnabled(cacheable=true)
    public static String generateCustomFieldMetadataXml(String objectApiName, String fieldName) {
        try {
            // Instantiate MetadataService.MetadataPort
            MetadataService.MetadataPort service = new MetadataService.MetadataPort();
            service.SessionHeader = new MetadataService.SessionHeader_element();
            
            service.SessionHeader.sessionId = getSessionId();

            // Retrieve the custom object fields metadata
            //MetadataService.CustomField customField = (MetadataService.CustomField) service.readMetadata('CustomField', 
            //                                                                                            new String[] { objectApiName + '.' + fieldName }).getRecords()[0];


            // Specify the type of metadata to read (CustomField) and the full name of the field
            String customFieldFullName = objectApiName + '.' + fieldName;
            String[] fullNames = new String[] { customFieldFullName };
            //MetadataService.Metadata[] metadata = service.readMetadata('CustomField', fullNames);
            MetadataService.IReadResult readResult = service.readMetadata('CustomField', fullNames);
            List<MetadataService.Metadata> metadata = readResult.getRecords();
            // Retrieve the custom field metadata
            MetadataService.CustomField customField = (MetadataService.CustomField) metadata[0];
            
            // Serialize the custom field metadata into XML
            //String xml = MetadataService.MetadataXmlSerializer.serialize(customField);
            // Construct the XML string manually
            String xml = '<?xml version="1.0" encoding="UTF-8"?>\n';
            xml += '<CustomField xmlns="http://soap.sforce.com/2006/04/metadata">\n';
            xml += '    <fullName>' + customField.fullName + '</fullName>\n';
            xml += '    <externalId>' + customField.externalId + '</externalId>\n';
            xml += '    <label>' + customField.label + '</label>\n';
            xml += '    <length>' + customField.length + '</length>\n';
            xml += '    <type>' + customField.type_x + '</type>\n';
            xml += '</CustomField>';

        
            return xml;
        } catch (Exception e) {
            // Handle exception as needed
            return null;
        }
    }


    // Function to check if folder path exists in the list of actions for a particular object name
    private static Boolean folderPathExistsInActions(List<Map<String, Object>> actions, String folderPath) {
        for (Map<String, Object> action : actions) {
            String actionPath = (String) action.get('file_path');
            if (actionPath != null && actionPath.equals(folderPath)) {
                return true;
            }
        }
        return false;
    }


    @AuraEnabled(cacheable=true)
    public static String openPullRequest(String userStoryId) {
        // Retrieve User Story details
        User_Story__c userStory = [SELECT Id, Name FROM User_Story__c WHERE Id = :userStoryId LIMIT 1];
        if (userStory == null) {
            return 'User Story not found.';
        }
        
        // Query for the Branching record where Assigned__c is checked
        Branching__c targetBranchRecord = [SELECT Id, Git_Branch__c FROM Branching__c WHERE Assigned__c = true LIMIT 1];
        if (targetBranchRecord == null) {
            return 'No assigned target branch found.';
        }
        
        // Define GitLab API endpoint, access token
        String gitLabApiEndpoint = 'https://gitlab.com/api/v4/projects/51918101/merge_requests';
        String gitLabAccessToken = 'glpat-E2kQJRyszgNmQAfVMrjd';
        
        // Prepare the HTTP request
        HttpRequest request = new HttpRequest();
        request.setEndpoint(gitLabApiEndpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + gitLabAccessToken);
        
        // Construct the commit message
        String commitMessage = 'Opening pull request for User Story: ' + userStory.Name;
        String sourceBranchName = 'feature/' + userStory.Name.replaceAll('\\s+', '-');
        // Prepare pull request payload
        Map<String, Object> pullRequestPayload = new Map<String, Object>();
        pullRequestPayload.put('source_branch', sourceBranchName);
        pullRequestPayload.put('target_branch', targetBranchRecord.Git_Branch__c);
        pullRequestPayload.put('title', commitMessage);
        pullRequestPayload.put('description', 'This pull request is associated with User Story ID: ' + userStoryId);
        
        // Send HTTP request
        String pullRequestPayloadJson = JSON.serialize(pullRequestPayload);
        request.setBody(pullRequestPayloadJson);
        
        Http http = new Http();
        HttpResponse response = http.send(request);
        
        if (response.getStatusCode() == 201) {
            return 'Pull request opened successfully!';
        } else {
            throw new AuraHandledException('Failed to open pull request: ' + response.getBody());
        }
    }


    @AuraEnabled
    public static String commitSelectedFieldsUsId(String userStoryId) {
        // Query User_Story_Metadata__c records related to the User_Story__c Id
        List<User_Story_Metadata__c> metadataList = [SELECT Id, Name, Field_API_Name__c, Field_Name__c, Field_Type__c, Object_Name__c, User_Story__c ,toDelete__c 
                                                    FROM User_Story_Metadata__c WHERE User_Story__c = :userStoryId AND Type__c = 'SO'];
        User_Story__c story = [SELECT Id, Name ,Step__c FROM User_Story__c WHERE Id = :userStoryId LIMIT 1];

        // Check if metadataList is empty
        if (metadataList.isEmpty()) {
            return 'No metadata records found for the provided User Story Id.';
        }

        // Define GitLab API endpoint, access token, and commit message
        String gitLabApiEndpoint = 'https://gitlab.com/api/v4/projects/51918101/repository/commits';
        String gitLabAccessToken = 'glpat-8XX3TYiCH_vmWRxgUFG8';
        String gitLabCommitMessage = 'Committing selected fields for User Story: ' + story.Name;

        // Create a new branch with the name of the user story
        String newBranchName = 'feature/' + story.Name.replaceAll('\\s+', '-'); // Replace spaces with hyphens
        createGitLabBranch(newBranchName, gitLabAccessToken); // Create branch using GitLab API

        // Prepare the HTTP request
        HttpRequest request = new HttpRequest();
        request.setEndpoint(gitLabApiEndpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + gitLabAccessToken);

        // Prepare commit payload
        Map<String, Object> commitPayload = new Map<String, Object>();
        commitPayload.put('branch', newBranchName); // Use the newly created branch
        commitPayload.put('commit_message', gitLabCommitMessage);

        List<Map<String, Object>> actions = new List<Map<String, Object>>();
        String membersContent = '';
        // Process metadata records
        for (User_Story_Metadata__c metadata : metadataList) {
            Map<String, Object> action = new Map<String, Object>();

             // Check if toDelete is true
            if (metadata.ToDelete__c) {
                // Append <members> tag content for deletion
                membersContent += '        <members>' + metadata.Object_Name__c + '.' + metadata.Field_API_Name__c + '</members>\n';
            } else {

            // Construct folder path with object name
            String folderPath = 'force-app/main/default/objects/' + metadata.Object_Name__c;
            String folderFieldsPath = 'force-app/main/default/objects/' + metadata.Object_Name__c + '/fields';
            String objectXmlFileName = metadata.Object_Name__c + '.object-meta.xml';
            String objectXmlContent = generateCustomObjectMetadataXml(metadata.Object_Name__c);
            String objectXmlFilePath = folderPath + '/' + objectXmlFileName;

            if (!folderPathExistsInActions(actions, objectXmlFilePath)) { 
                // Add action for object XML file
                action.put('action', 'create');
                action.put('file_path', objectXmlFilePath);
                action.put('content', objectXmlContent);
                actions.add(action);
            }

            // Add actions for field XML files
            String fieldXmlFileName = metadata.Field_API_Name__c + '.field-meta.xml';
            String fieldXmlContent = generateCustomFieldMetadataXml(metadata.Object_Name__c, metadata.Field_API_Name__c);
            String fieldXmlFilePath = folderFieldsPath + '/' + fieldXmlFileName;

            // Add action for field XML file
            action = new Map<String, Object>();
            action.put('action', 'create');
            action.put('file_path', fieldXmlFilePath);
            action.put('content', fieldXmlContent);
            actions.add(action);
        }
     }
        String XmlContent = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
            '<Package xmlns="http://soap.sforce.com/2006/04/metadata">\n' +
            '    <types>\n' +
            + membersContent +// Add accumulated members content here
            '        <name>CustomField</name>\n' +
            '    </types>\n' +
            '    <version>58.0</version>\n' +
            '</Package>';

        String XmlFilePath = 'manifest/package.xml';

        Map<String, Object> action = new Map<String, Object>();
        action.put('action', 'create');
        action.put('file_path', XmlFilePath);
        action.put('content', XmlContent);
        actions.add(action);

        commitPayload.put('actions', actions);

        // Send HTTP request
        String commitPayloadJson = JSON.serialize(commitPayload);
        request.setBody(commitPayloadJson);

        Http http = new Http();
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 201) {

            // Update the Step__c field of the User_Story__c record to 'USB'
            story.Step__c = 'USB';
            update story; // Update the record


            return 'Commit successful!';
        } else {
            throw new AuraHandledException('GitLab Commit failed: ' + response.getBody());
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<apexClass> getAllApexClasses() {
        // List<ObjectMetadata> apexClasses = new List<ObjectMetadata>();
        
        // // Query all Apex classes
        // for (ApexClass apexClass : [SELECT Name FROM ApexClass]) {
        //     ObjectMetadata classMetadata = new ObjectMetadata();
        //     classMetadata.objectName = apexClass.Name;
        //     apexClasses.add(classMetadata);
        // }
        
        // return apexClasses;
        return [SELECT Id,Name,IsValid, Status FROM ApexClass];
    }
    
    @AuraEnabled(cacheable=true)
    public static String getApexClassContent(String className) {
        try {
            // Query for the Apex class content
            ApexClass apexClass = [SELECT Body FROM ApexClass WHERE Name = :className LIMIT 1];
            return apexClass.Body;
        } catch (Exception e) {
            // Handle exception as needed
            return null;
        }
    }

    @AuraEnabled
    public static String commitSelectedApexClassesUsId(String userStoryId) {
        // Query User_Story_Metadata__c records related to the User_Story__c Id
        List<User_Story_Metadata__c> metadataList = [SELECT Id, Name, Class_Name__c, User_Story__c ,toDelete__c 
                                                    FROM User_Story_Metadata__c WHERE User_Story__c = :userStoryId AND Type__c = 'AC'];
        User_Story__c story = [SELECT Id, Name ,Step__c FROM User_Story__c WHERE Id = :userStoryId LIMIT 1];

        // Check if metadataList is empty
        if (metadataList.isEmpty()) {
            return 'No metadata records found for the provided User Story Id.';
        }

        // Define GitLab API endpoint, access token, and commit message
        String gitLabApiEndpoint = 'https://gitlab.com/api/v4/projects/51918101/repository/commits';
        String gitLabAccessToken = 'glpat-8XX3TYiCH_vmWRxgUFG8';
        String gitLabCommitMessage = 'Committing selected Apex Classes for User Story: ' + story.Name;

        // Create a new branch with the name of the user story
        String newBranchName = 'feature/' + story.Name.replaceAll('\\s+', '-'); // Replace spaces with hyphens
        createGitLabBranch(newBranchName, gitLabAccessToken); // Create branch using GitLab API

        // Prepare the HTTP request
        HttpRequest request = new HttpRequest();
        request.setEndpoint(gitLabApiEndpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + gitLabAccessToken);

        // Prepare commit payload
        Map<String, Object> commitPayload = new Map<String, Object>();
        commitPayload.put('branch', newBranchName); // Use the newly created branch
        commitPayload.put('commit_message', gitLabCommitMessage);

        List<Map<String, Object>> actions = new List<Map<String, Object>>();
        String membersContent = '';
        // Process metadata records
        for (User_Story_Metadata__c metadata : metadataList) {
            Map<String, Object> action = new Map<String, Object>();

             // Check if toDelete is true
            if (metadata.ToDelete__c) {
                // Append <members> tag content for deletion
                membersContent += '        <members>' + metadata.Class_Name__c + '</members>\n';
            } else {

            // Construct folder path with object name
            String folderPath = 'force-app/main/default/classes/' + metadata.Class_Name__c;
            String folderFieldsPath = 'force-app/main/default/classes/' + metadata.Class_Name__c + '/fields';
            String objectXmlFileName = metadata.Class_Name__c + '.cls';
            String objectXmlContent = getApexClassContent(metadata.Class_Name__c);
            String objectXmlFilePath = folderPath + '/' + objectXmlFileName;

            if (!folderPathExistsInActions(actions, objectXmlFilePath)) { 
                // Add action for object XML file
                action.put('action', 'create');
                action.put('file_path', objectXmlFilePath);
                action.put('content', objectXmlContent);
                actions.add(action);
            }

            
        }
     }
        String XmlContent = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
            '<Package xmlns="http://soap.sforce.com/2006/04/metadata">\n' +
            '    <types>\n' +
            + membersContent +// Add accumulated members content here
            '        <name>ApexClass</name>\n' +
            '    </types>\n' +
            '    <version>58.0</version>\n' +
            '</Package>';

        String XmlFilePath = 'manifest/package.xml';

        Map<String, Object> action = new Map<String, Object>();
        action.put('action', 'create');
        action.put('file_path', XmlFilePath);
        action.put('content', XmlContent);
        actions.add(action);

        commitPayload.put('actions', actions);

        // Send HTTP request
        String commitPayloadJson = JSON.serialize(commitPayload);
        request.setBody(commitPayloadJson);

        Http http = new Http();
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 201) {

            // Update the Step__c field of the User_Story__c record to 'USB'
            story.Step__c = 'USB';
            update story; // Update the record


            return 'Commit successful!';
        } else {
            throw new AuraHandledException('GitLab Commit failed: ' + response.getBody());
        }
    }

    // Method to create a new branch using GitLab API
    private static void createGitLabBranch(String branchName, String accessToken) {
        String gitLabApiEndpoint = 'https://gitlab.com/api/v4/projects/51918101/repository/branches';

        HttpRequest request = new HttpRequest();
        request.setEndpoint(gitLabApiEndpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + accessToken);

        Map<String, String> requestBody = new Map<String, String>();
        requestBody.put('branch', branchName);
        requestBody.put('ref','blank'); // Create branch from master

        request.setBody(JSON.serialize(requestBody));

        Http http = new Http();
        HttpResponse response = http.send(request);

        if (response.getStatusCode() != 201) {
            throw new AuraHandledException('Failed to create GitLab branch: ' + response.getBody());
        }
    }


}