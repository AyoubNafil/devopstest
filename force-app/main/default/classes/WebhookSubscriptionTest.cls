@isTest
private class WebhookSubscriptionTest {

    @isTest
    static void testHandleNotification_ValidToken() {
     // Set up test data
        String requestBody = '{"commits":[{"id":"commit1","message":"Test commit","timestamp":"2024-01-24T12:00:00Z","url":"https://example.com/commit1","author":{"name":"Test Author","email":"test@example.com"},"added":["file1.txt"],"removed":[],"modified":["file2.txt"],"repository":{"name":"Test Repository"}}]}';
        
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/api/Webhooks/pushDetails';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.addHeader('X-Gitlab-Token', 'webhooksecret');
        request.requestBody = Blob.valueOf(requestBody);
        
        RestContext.request = request;

        // Call the method to be tested
        Test.startTest();
        String responseMessage = webhookSubscription.handleNotification();
        Test.stopTest();

        // Verify the response message
        System.assertEquals('Records inserted successfully', responseMessage, 'Expected successful insertion message');
        
        // Verify that the GitHub_Push_Details__c records are created
        List<GitHub_Push_Details__c> pushDetailsList = [SELECT Id FROM GitHub_Push_Details__c];
        System.assertEquals(1, pushDetailsList.size(), 'Expected one GitHub_Push_Details__c record to be created');
        GitHub_Push_Details__c pushDetails = pushDetailsList[0];
        System.assertEquals('commit1', pushDetails.Commit_ID__c, 'Expected Commit ID to match');
        System.assertEquals('Test Author', pushDetails.Committer_Name__c, 'Expected Committer Name to match');
        System.assertEquals('test@example.com', pushDetails.Committer_Email__c, 'Expected Committer Email to match');
        System.assertEquals('Test commit', pushDetails.Committer_Comment__c, 'Expected Commit Comment to match');
        System.assertEquals(DateTime.valueOf('2024-01-24 12:00:00'), pushDetails.Committed_Time__c, 'Expected Committed Time to match');
        System.assertEquals('https://example.com/commit1', pushDetails.Committed_Url__c, 'Expected Committed Url to match');
        System.assertEquals('Test Repository', pushDetails.Repository_Name__c, 'Expected Repository Name to match');
        System.assertEquals('file1.txt', pushDetails.Added_Components__c, 'Expected Added Components to match');
        System.assertEquals('', pushDetails.Removed_Components__c, 'Expected Removed Components to be empty');
        System.assertEquals('file2.txt', pushDetails.Modified_Components__c, 'Expected Modified Components to match');
    }

    @isTest
    static void testHandleNotification_InvalidToken() {
        // Set up invalid token
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/api/Webhooks/pushDetails';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.addHeader('X-Gitlab-Token', 'invalidtoken');
        request.requestBody = Blob.valueOf('{}');

        RestContext.request = request;

        // Call the method to be tested
        Test.startTest();
        String responseMessage = webhookSubscription.handleNotification();
        Test.stopTest();

        // Verify the response message for invalid token
        System.assertEquals('Error: Invalid Gitlab Token', responseMessage, 'Expected error message for invalid token');
    }
    
    

    
}