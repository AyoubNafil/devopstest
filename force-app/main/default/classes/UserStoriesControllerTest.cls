@isTest
private class UserStoriesControllerTest {

    @isTest
    static void testGetUserStory() {
        // Create test data
        User_Story__c userStory = new User_Story__c(Name = 'Test Story');
        insert userStory;

        // Call the method being tested
        List<User_Story__c> result = UserStoriesController.GetUserStory(userStory.Id);

        // Assert that the method returns the expected result
        System.assertEquals(1, result.size());
        System.assertEquals('Test Story', result[0].Name);
    }

    @isTest
    static void testInsertUserStoryMetadata() {
        // Create test data
        List<User_Story_Metadata__c> metadataList = new List<User_Story_Metadata__c>();
        metadataList.add(new User_Story_Metadata__c(
            Name = 'Test Metadata',
            Field_API_Name__c = 'Test_Field_API_Name',
            Field_Name__c = 'Test_Field_Name',
            Field_Type__c = 'Text',
            Object_Name__c = 'Test_Object',
            User_Story__c = 'a6P8e00000111bfEAA' // Assuming a valid User_Story__c Id
        ));

        // Call the method being tested
        Test.startTest();
        UserStoriesController.insertUserStoryMetadata(metadataList);
        Test.stopTest();

        // Retrieve inserted metadata
        List<User_Story_Metadata__c> insertedMetadata = [SELECT Id FROM User_Story_Metadata__c WHERE Id IN :metadataList];

        // Assert that metadata is inserted
        System.assertEquals(1, insertedMetadata.size());
    }

    @isTest
    static void testGetUserStoryMetadata() {
        // Create test data
        User_Story__c userStory = new User_Story__c(Name = 'Test Story');
        insert userStory;

        User_Story_Metadata__c metadata = new User_Story_Metadata__c(
            Name = 'Test Metadata',
            Field_API_Name__c = 'Test_Field_API_Name',
            Field_Name__c = 'Test_Field_Name',
            Field_Type__c = 'Text',
            Object_Name__c = 'Test_Object',
            User_Story__c = userStory.Id
        );
        insert metadata;

        // Call the method being tested
        List<User_Story_Metadata__c> result = UserStoriesController.GetUserStoryMetadata(userStory.Id);

        // Assert that the method returns the expected result
        System.assertEquals(1, result.size());
        System.assertEquals('Test Metadata', result[0].Name);
    }
}