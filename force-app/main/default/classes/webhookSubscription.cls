@RestResource(urlMapping='/api/Webhooks/pushDetails/*')
global without sharing class webhookSubscription {

    private static final String EXPECTED_GITLAB_TOKEN = 'webhooksecret';

    private class Author {
        String name;
        String email;
    }

    private class Repository {
        String name;
    }

    private class CommitInfo {
        String id; // Add Commit ID
        String message;
        String timestamp;
        String url;
        Author author;
        List<String> added;
        List<String> removed;
        List<String> modified;
    }

    private class GitHubPushDetails {
        List<CommitInfo> commits;
        Repository repository;
    }

    @HttpPost
    global static String handleNotification() {
        String responseMessage;

        try {
            // Check the X-Gitlab-Token header
            String gitlabToken = RestContext.request.headers.get('X-Gitlab-Token');
            
            if (gitlabToken == null || !gitlabToken.equals(EXPECTED_GITLAB_TOKEN)) {
                throw new CustomException('Invalid Gitlab Token');
            }

            String requestBody = RestContext.request.requestBody.toString();
            GitHubPushDetails gitHubPushDetails = (GitHubPushDetails) JSON.deserialize(requestBody, GitHubPushDetails.class);

            Repository repository = gitHubPushDetails.repository;

            //Set<String> nonExistingCommitIds = getNonExistingCommitIds(gitHubPushDetails.commits);

            List<GitHub_Push_Details__c> pushDetailsList = new List<GitHub_Push_Details__c>();

            for (CommitInfo commitInfo : gitHubPushDetails.commits) {
                // Check if a record with the same commit ID already exists
                //if (nonExistingCommitIds.contains(commitInfo.id)) {
                   
                    GitHub_Push_Details__c pushDetails = new GitHub_Push_Details__c();
                    pushDetails.Commit_ID__c = commitInfo.id;
                    pushDetails.Committer_Name__c = commitInfo.author.name;
                    pushDetails.Committer_Email__c = commitInfo.author.email;
                    pushDetails.Committer_Comment__c = commitInfo.message;
                    pushDetails.Committed_Time__c = DateTime.valueOfGmt(commitInfo.timestamp.replace('T', ' ').replace('+', ' '));
                    pushDetails.Committed_Url__c = commitInfo.url;
                    pushDetails.Repository_Name__c = repository.name;

                    pushDetails.Added_Components__c = String.join(commitInfo.added, ';');
                    pushDetails.Removed_Components__c = String.join(commitInfo.removed, ';');
                    pushDetails.Modified_Components__c = String.join(commitInfo.modified, ';');

                    pushDetailsList.add(pushDetails);
                //} else {
                //    System.debug('Skipping duplicate commit: ' + commitInfo.id);
                //}
            }

            if (!pushDetailsList.isEmpty()) {
                //insert pushDetailsList;
                // Use upsert to insert or update records based on the Commit_ID__c field
                Database.upsert(pushDetailsList, GitHub_Push_Details__c.Fields.Commit_ID__c, false);
                responseMessage = 'Records inserted successfully';
            } else {
                responseMessage = 'No records to insert';
            }

        } catch (CustomException ce) {
            responseMessage = 'Error: ' + ce.getMessage();
        } catch (Exception e) {
            responseMessage = 'Error: ' + e.getMessage();
        }

        return responseMessage;
    }

    // private static Set<String> getNonExistingCommitIds(List<CommitInfo> newCommits) {
    //     Set<String> nonExistingCommitIds = new Set<String>();

    //     for (CommitInfo newCommit : newCommits) {
    //         nonExistingCommitIds.add(newCommit.id);
    //     }

    //     List<GitHub_Push_Details__c> existingRecords = [SELECT Commit_ID__c FROM GitHub_Push_Details__c];
        
    //     for (GitHub_Push_Details__c existingRecord : existingRecords) {
    //         nonExistingCommitIds.remove(existingRecord.Commit_ID__c);
    //     }

    //     System.debug('+++++++++++++++++++++++++nonExistingCommitIds++++++++++++++++++');
    //     System.debug(nonExistingCommitIds);

    //     return nonExistingCommitIds;
    // }

    private class CustomException extends Exception {
       
    }
}