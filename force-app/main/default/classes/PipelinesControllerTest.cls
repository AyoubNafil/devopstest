@isTest
private class PipelinesControllerTest {
    @isTest
    static void testGetPipelineStages() {
        // Create test data
        Pipeline__c testPipeline = new Pipeline__c(Name = 'Test Pipeline');
        insert testPipeline;
        
        Stage__c testStage1 = new Stage__c(Name = 'Stage 1', Pipeline__c = testPipeline.Id);
        Stage__c testStage2 = new Stage__c(Name = 'Stage 2', Pipeline__c = testPipeline.Id);
        insert new List<Stage__c>{testStage1, testStage2};
        
        // Call the method to be tested
        Test.startTest();
        List<Stage__c> stages = PipelinesController.getPipelineStages(testPipeline.Id);
        Test.stopTest();
        
        // Verify the results
        System.assertEquals(2, stages.size(), 'Expected two stages for the test pipeline');
        System.assertEquals('Stage 1', stages[0].Name, 'Expected first stage name to be Stage 1');
        System.assertEquals('Stage 2', stages[1].Name, 'Expected second stage name to be Stage 2');
    }
}