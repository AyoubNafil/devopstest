public with sharing class DestinationOrgController {
    @AuraEnabled
    public static void createDestinationOrgRecord(Id destinationId, Id sourceId) {
        try {
            // Perform validation checks if necessary

            // Create DestinationOrg__c record
            DestinationOrg__c newRecord = new DestinationOrg__c(
                Destination__c = destinationId,
                Source__c = sourceId
            );
            insert newRecord;
        } catch (Exception ex) {
            throw new AuraHandledException('Error creating DestinationOrg__c record: ' + ex.getMessage());
        }
    }


}
