@isTest
public class GitLabCommitControllerMock implements HttpCalloutMock {
    public HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setStatusCode(201);
        res.setBody('{"message":"Commit successful!"}');
        return res;
    }
}