public without sharing class GetSObjectWithFields {


    @AuraEnabled(cacheable=true)
    public static List<ObjectFieldWrapper> getCustomObjectFields() {
    List<ObjectFieldWrapper> objectFields = new List<ObjectFieldWrapper>();

    Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    Map<String, SObjectType> customObjects = new Map<String, SObjectType>();

    for (String objectName : schemaMap.keySet()) {
        if (!objectName.startsWith('vlocity_cmt')) {
            SObjectType objectType = schemaMap.get(objectName);
            customObjects.put(objectName, objectType);
        }
    }

    for (String objectName : customObjects.keySet()) {
        Map<String, Schema.SObjectField> fieldMap = customObjects.get(objectName).getDescribe().fields.getMap();

        for (Schema.SObjectField sfield : fieldMap.values()) {
            Schema.DescribeFieldResult dfield = sfield.getDescribe();
            ObjectFieldWrapper fieldWrapper = new ObjectFieldWrapper();
            fieldWrapper.objectName = objectName;
            fieldWrapper.fieldName = dfield.getLabel();
            fieldWrapper.fieldAPIName = dfield.getName();
            fieldWrapper.fieldType = String.valueOf(dfield.getType());
            objectFields.add(fieldWrapper);
        }
    }

    return objectFields;
}


    // @AuraEnabled(cacheable=true)
    // public static List<ObjectFieldWrapper> getCustomObjectFields() {
    //     List<ObjectFieldWrapper> objectFields = new List<ObjectFieldWrapper>();
    //     Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();

    //     for (String objectName : schemaMap.keySet()) {
    //         //if (objectName.endsWith('__c') && !objectName.startsWith('vlocity_cmt')) {
    //         if (!objectName.startsWith('vlocity_cmt')) {
    //             Map<String, Schema.SObjectField> fieldMap = schemaMap.get(objectName).getDescribe().fields.getMap();

    //             for (Schema.SObjectField sfield : fieldMap.values()) {
    //                 Schema.DescribeFieldResult dfield = sfield.getDescribe();
    //                 ObjectFieldWrapper fieldWrapper = new ObjectFieldWrapper();
    //                 fieldWrapper.objectName = objectName;
    //                 fieldWrapper.fieldName = dfield.getLabel();
    //                 fieldWrapper.fieldAPIName = dfield.getName();
    //                 fieldWrapper.fieldType = String.valueOf(dfield.getType());
    //                 objectFields.add(fieldWrapper);
    //             }
    //         }
    //     }

    //     return objectFields;
    // }

    public class ObjectFieldWrapper {
        @AuraEnabled
        public String objectName { get; set; }

        @AuraEnabled
        public String fieldName { get; set; }

        @AuraEnabled
        public String fieldAPIName { get; set; }

        @AuraEnabled
        public String fieldType { get; set; }
    }
}