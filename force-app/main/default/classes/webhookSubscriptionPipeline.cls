@RestResource(urlMapping='/api/Webhooks/pipelineDetails/*')
global without sharing class webhookSubscriptionPipeline {

    private static final String EXPECTED_GITLAB_TOKEN = 'webhooksecret';

    private class ObjectAttributes {
        String id;
        String status;
        String detailed_status;
        String[] stages;
        String created_at;
        String finished_at;
        Decimal duration;
        Decimal queued_duration;
        String url;


        // Helper method to convert duration to a string representation (mm:ss)
        String getFormattedDuration() {
            Integer totalSeconds = duration != null ? duration.intValue() : 0;
            Integer minutes = totalSeconds / 60;
            Integer seconds = totalSeconds - (minutes * 60);
            return minutes + ' min : ' + seconds + ' sec';
        }

        // Helper method to convert queued duration to a string representation (mm:ss)
        String getFormattedQueuedDuration() {
            Integer totalSeconds = queued_duration != null ? queued_duration.intValue() : 0;
            Integer minutes = totalSeconds / 60;
            Integer seconds = totalSeconds - (minutes * 60);
            return minutes + ' min : ' + seconds + ' sec';
        }


    }

    private class MergeRequest {
        // Define MergeRequest fields if needed
    }

    private class User {
        // Define User fields if needed
    }

    private class Project {
        String name;
        // Define Project fields if needed
    }

    private class Commit_x {
        String title;
        // Define Commit fields if needed
    }

    private class Build {
        String id;
        String stage;
        String name;
        String status;
        String created_at;
        String started_at;
        String finished_at;
        Decimal duration;
        Decimal queued_duration;
        String allow_failure;
        User user;
        // Add more fields as needed

        // Helper method to convert duration to a string representation (mm:ss)
        String getFormattedDuration() {
            Integer totalSeconds = duration != null ? duration.intValue() : 0;
            Integer minutes = totalSeconds / 60;
            Integer seconds = totalSeconds - (minutes * 60);
            return minutes + ' min : ' + seconds + ' sec';
        }
        // Helper method to convert queued duration to a string representation (mm:ss)
        String getFormattedQueuedDuration() {
            Integer totalSeconds = queued_duration != null ? queued_duration.intValue() : 0;
            Integer minutes = totalSeconds / 60;
            Integer seconds = totalSeconds - (minutes * 60);
            return minutes + ' min : ' + seconds + ' sec';
        }
    }

    private class PipelineDetails {
        String object_kind;
        ObjectAttributes object_attributes;
        MergeRequest merge_request;
        User user;
        Project project;
        Commit_x commit_x;
        List<Build> builds;


        
    }

    

    @HttpPost
    global static String handlePipelineNotification() {
        String responseMessage;
        Id PipelineID;

        try {
            // Check the X-Gitlab-Token header
            String gitlabToken = RestContext.request.headers.get('X-Gitlab-Token');
            
            if (gitlabToken == null || !gitlabToken.equals(EXPECTED_GITLAB_TOKEN)) {
                throw new PipelineWebhookException('Invalid Gitlab Token');
            }

            String requestBody = RestContext.request.requestBody.toString();
            String requestBodyStringSafe = requestBody.replaceAll('\"commit\":', '\"commit_x\":');
            PipelineDetails pipelineDetails = (PipelineDetails) JSON.deserialize(requestBodyStringSafe, PipelineDetails.class);

            // Create Pipeline__c record
            Pipeline__c pipelineRecord = new Pipeline__c();
            pipelineRecord.Gitlab_Id__c = String.valueOf(pipelineDetails.object_attributes.id);
            pipelineRecord.Name = pipelineDetails.commit_x.title;
            pipelineRecord.Status__c = pipelineDetails.object_attributes.status;
            pipelineRecord.Detailed_Status__c = pipelineDetails.object_attributes.detailed_status;
            pipelineRecord.Created_At__c = DateTime.valueOfGmt(pipelineDetails.object_attributes.created_at.replace('T', ' ').replace('Z', ' '));
            pipelineRecord.Finished_At__c = DateTime.valueOfGmt(pipelineDetails.object_attributes.finished_at.replace('T', ' ').replace('Z', ' '));
            //pipelineRecord.Duration__c = pipelineDetails.object_attributes.duration;
            pipelineRecord.Duration__c = pipelineDetails.object_attributes.getFormattedDuration();
            //pipelineRecord.Queued_Duration__c = pipelineDetails.object_attributes.queued_duration;
            pipelineRecord.Queued_Duration__c = pipelineDetails.object_attributes.getFormattedQueuedDuration();
            pipelineRecord.Url__c = pipelineDetails.object_attributes.url;
            // Add more fields as needed

            //insert pipelineRecord;

            //responseMessage = pipelineRecord.Id;

            Database.upsert(pipelineRecord, Pipeline__c.Fields.Gitlab_Id__c, false);

            // Create Stage__c records based on Build objects
            List<Stage__c> stageRecords = new List<Stage__c>();

            for (Build build : pipelineDetails.builds) {
                Stage__c stageRecord = new Stage__c();
                

                //responseMessage = pipelineRecord.Id;

                stageRecord.Pipeline__c = pipelineRecord.Id;
                stageRecord.Gitlab_Id__c = String.valueOf(build.id);
                stageRecord.Name = build.stage;
                stageRecord.Build_Status__c = build.status;
                stageRecord.Started_At__c = DateTime.valueOfGmt(build.started_at.replace('T', ' ').replace('Z', ' '));
                stageRecord.Finished_At__c = DateTime.valueOfGmt(build.finished_at.replace('T', ' ').replace('Z', ' '));
                //stageRecord.Duration__c = build.duration;
                stageRecord.Duration__c = build.getFormattedDuration();
                //stageRecord.Queued_Duration__c = build.queued_duration;
                stageRecord.Queued_Duration__c = build.getFormattedQueuedDuration();
                // // Add more fields as needed

                stageRecords.add(stageRecord);
            }

            if (!stageRecords.isEmpty()) {
                //insert stageRecords;
                Database.upsert(stageRecords, Stage__c.Fields.Gitlab_Id__c, false);
            }

            responseMessage = 'Pipeline records processed successfully';

        } catch (PipelineWebhookException pwe) {
            responseMessage = 'Error: ' + pwe.getMessage();
        } catch (Exception e) {
            responseMessage = 'Error: ' + e.getMessage();
        }

        return responseMessage;
    }


    private class PipelineWebhookException extends Exception {

    }
}