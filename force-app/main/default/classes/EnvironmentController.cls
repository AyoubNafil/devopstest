public with sharing class EnvironmentController {
    @AuraEnabled(cacheable=true)
    public static List<Environment__c> getEnvironments() {
        return [SELECT Id, Name FROM Environment__c];
    }
}