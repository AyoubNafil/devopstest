public without sharing class PipelinesController {
    @AuraEnabled(cacheable=true)
    public static List<Stage__c> getPipelineStages(String PipId) {
       
        
        return [SELECT Id, Name,Build_Status__c
                FROM Stage__c
                where Pipeline__c=: PipId] ;
    }
}